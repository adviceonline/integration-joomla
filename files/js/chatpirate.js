/* API SERVER URL */
var server_url = 'https://app.chatpirate.com/';

/* AJAX LOADING*/
jQuery(document)
	.ajaxStart(function () {
		jQuery('body').css("overflow", "hidden");
		jQuery('.loading').fadeIn( "slow");
	})
	.ajaxStop(function () {
		jQuery('body').css("overflow", "visible");
		jQuery('.loading').fadeOut( "slow");
	});

/* EMAIL VALIDATION */
function IsEmail(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+jQuery/;
	return regex.test(email);
}

/* LOAD CSS OR JS FILE */
function loadjscssfile(filename, filetype) {
	if (filetype == "js") {
		var fileref = document.createElement('script')
		fileref.setAttribute("type", "text/javascript")
		fileref.setAttribute("src", filename)
	}
	else if (filetype == "css") {
		var fileref = document.createElement("link")
		fileref.setAttribute("rel", "stylesheet")
		fileref.setAttribute("type", "text/css")
		fileref.setAttribute("href", '../modules/mod_chatpirate/files/' + filename)
	}
	if (typeof fileref != "undefined")
		document.getElementsByTagName("head")[0].appendChild(fileref)
}

jQuery( document ).ready(function() {

	/* LOAD STYLE CSS */
	loadjscssfile('css/chatpirate.css', 'css');

	jQuery( "#save_chatpirate_setting" ).click(function() {
		saveSettings();
	});

});


/* SAVE SETTINGS */
function saveSettings(){
	var chatpirate_email    = jQuery( "#jform_params_chatpirate_email").val(),
		chatpirate_password = jQuery( "#jform_params_chatpirate_password").val(),
		errors = false;

	jQuery('#notifications').html('');

	/* SIMPLE VALIDATION */
	if(!chatpirate_email) {
		displayMessage('E-mail can not be empty', 'error');
		errors = true;
	}
	if(IsEmail(chatpirate_email)){
		displayMessage('E-mail address is not valid', 'error');
		errors = true;
	}
	if (!chatpirate_password) {
		displayMessage('Password can not be empty', 'error');
		errors = true;
	}
	if(chatpirate_password.length < 6)
	{
		displayMessage('Your password must be at least 6 characters', 'error');
		errors = true;
	}
	if(errors == false){
		getToken(chatpirate_email, chatpirate_password);
	}
};


/* GET USER TOKEN */
function getToken(email, password) {
	jQuery.ajax({
		method: "GET",
		url: server_url+"api/v1/register/getToken",
		headers: {
			'Authorization': email+'::'+password,
		},
		success: function (res) {
			jQuery( "#jform_params_chatpirate_companyid").val(res.data.companyId);
			jQuery( "#jform_params_chatpirate_password").val('');
			Joomla.submitform('module.apply', document.getElementById('module-form'));
		},
		error: function (res, xx, yy) {
			displayMessage(yy, 'error');
		}
	});
}

/* DISPLAY NOTIFICATIONS */
function displayMessage(message, type){
	switch (type) {
		case "error":
			jQuery('#notifications').append('<div class="alert error">'+message+'</div>');
			break;
		case "success":
			jQuery('#notifications').append('<div class="alert success">'+message+'</div>');
			break;
	}
}

Joomla.submitbutton = function(task) {

	if (task == "module.cancel") {
		Joomla.submitform(task, document.getElementById('module-form'));
		return false;
	} else {
		saveSettings();
	}

}
<?php
/**
 * @version		1.0.0
 * @package		ChatPirate
 * @copyright	Copyright (C) 2015 ChatPirate (chatpirate.com). All rights reserved.
 * @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */

defined('_JEXEC') or die;

$companyid = $params->get('chatpirate_companyid');
if(!empty($companyid))
{
	echo "<script type=\"text/javascript\">
                    var __cp = {};
                    __cp.license = ".$companyid.";

                    (function(){
                        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
                        s.src = ('https:' == window.location.protocol ? 'https://' : 'http://') + 'cdn.chatpirate.com/plugin.js';
                        var sc = document.getElementsByTagName('script')[0]; sc.parentNode.insertBefore(s, sc);
                    })();
            </script>";
}
?>
